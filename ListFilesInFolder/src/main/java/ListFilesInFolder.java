
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ListFilesInFolder {
 
 public static void main(String[] args) {
	 		// Directory path here
		  String path = args[0]; 
		 
		  File folder = new File(path);
		  File[] listOfFiles = folder.listFiles(); 
		  PrintWriter writer;
		try {
			writer = new PrintWriter("outputFile.txt", "UTF-8");
			for (File file : listOfFiles) {
				  writer.println(file.getName());
			  }
			 writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		 
	}
}
